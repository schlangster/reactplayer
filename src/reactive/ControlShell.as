﻿import reactive.ReactPlayer;
import reactive.GlobalFunctions;

class reactive.ControlShell
{
	public var inputField: TextField;
	public var errorField: TextField;
	public var statusField: TextField;
	
	public var player: ReactPlayer;
	
	private var _lastInput: String = "";
	
	public function ControlShell()
	{		
		Key.addListener(this);
		Selection.addListener(this);
		
		Selection.setFocus(inputField);
	}
	
	public function onKeyUp(): Void
	{
		if (Key.getCode() == Key.ENTER)
		{
			var s = inputField.text;
			if (s == "")
				return;
			inputField.text = "";
			_lastInput = s;
			parseInput(s);
		}
		else if (Key.getCode() == Key.UP)
		{
			inputField.text = _lastInput;
		}
	}
	
	public function onSetFocus(): Void
	{
		if (Selection.getFocus() != inputField)
			Selection.setFocus(inputField);
	}
	
	private function parseInput(a_str: String): Void
	{
		clearStatus();
		
		var s = a_str;
		
		do
		{
			var t = s.split("  ").join(" ");
			
			if (s.length == t.length)
				break;
				
			s = t;
		}
		while (true);
		
		var inputs =  s.split(" ");
			
		// Lazy
		var cmd = inputs[0];
		var arg1 = inputs[1];
		var arg2 = inputs[2];
		var arg3 = inputs[3];
		var arg4 = inputs[4];
		
		if (cmd == "load")
		{
			if (!arg1)
			{
				printError("load: log name missing");
				return;
			}
			
			player.cmdLoad(arg1);
		}
		else if (cmd == "play")
		{
			player.cmdPlay(arg1);
		}
		else if (cmd == "pause")
		{
			player.cmdPause();
		}
		else if (cmd == "reload")
		{
			player.cmdReload();
		}
		else if (cmd == "step")
		{
			player.cmdStep();
		}
		else if (cmd == "debugmode")
		{
			player.cmdDebugMode(arg1 == "1");
		}
		else if (cmd == "edgestyle")
		{
			if (!arg1)
			{
				printError("edgestyle: style id missing");
				return;
			}
			
			player.cmdEdgeStyle(Number(arg1));
		}
		else if (cmd == "showthreads")
		{
			if (!arg1)
			{
				printError("showthreads: transaction id missing");
				return;
			}
			
			player.cmdShowThreads(arg1);
		}
		else if (cmd == "clearthreads")
		{
			player.cmdClearThreads();
		}
		else if (cmd == "")
		{
		}
		else
		{
			printError("error: unknown command '" + cmd + "'");
		}
	}
	
	public function clearStatus(): Void
	{
		statusField.text = "";
		errorField.text = "";
	}
	
	public function printError(a_str: String): Void
	{
		statusField.text = "";
		errorField.text = a_str;
	}
	
	public function printInfo(a_str: String): Void
	{
		errorField.text = "";
		statusField.text = a_str;
	}
}