﻿import reactive.NodeGroup;

class reactive.Node extends MovieClip
{
	public var typeTextField: TextField;
	public var transactionTextField: TextField;
	
	public var idlePulseAnim: MovieClip;
	public var pulseAnim: MovieClip;
	public var invalidateAnim: MovieClip;
	
	public var background: MovieClip;
	
	public var group: NodeGroup = null;
	
	public var outEdges: Array;
	public var inEdges: Array;
	
	public var id: Number;
	
	public var threadMap: Object;
	
	private var _resetCounter: Number = 0;
	
	public function Node()
	{
		reset();
		
		outEdges = [];
		inEdges = [];
		
		threadMap = {};
	}
	
	public function get typeTag(): String
	{
		return typeTextField.text;
	}
	
	public function set typeTag(a_tag: String): Void
	{
		typeTextField.text = a_tag;
	}
	
	public function onEdgeReset(): Void
	{
		if (_resetCounter > 0)
		{
			_resetCounter--
			if (_resetCounter == 0)
				reset();
		}
	}
	
	public function showThread(a_transaction: Number)
	{
		
	}
	
	public function reset(): Void
	{
		transactionTextField._visible = false;
		transactionTextField.text = "";
		background.gotoAndStop("idle");
		idlePulseAnim.gotoAndStop("hide");
		pulseAnim.gotoAndStop("hide");
		invalidateAnim.gotoAndStop("hide");
		
		for (var i=0; i<outEdges.length; i++)
			outEdges[i].onReset();
	}
	
	public function startProcessing(a_transaction: Number, a_threadNum: Number): Void
	{
		transactionTextField._visible = true;
		transactionTextField.text = String(a_transaction);
		transactionTextField.textColor = 0x990000;
		background.gotoAndStop("processing");
		
		threadMap[a_transaction] = a_threadNum;
	}
	
	public function stopProcessing(): Void
	{			
		background.gotoAndStop("idle");
		
		for (var i=0; i<inEdges.length; i++)
			inEdges[i].onReset();
	}
	
	public function setAdmission(a_transaction: Number): Void
	{
		background.gotoAndStop("admission");
		
		transactionTextField._visible = true;
		transactionTextField.text = String(a_transaction);
		transactionTextField.textColor = 0x990000;
	}
	
	public function idlePulse(a_transaction: Number): Void
	{
		reset();
		idlePulseAnim.gotoAndPlay("pulse");
	}
	
	public function pulse(a_transaction: Number): Void
	{
		invalidateAnim.gotoAndStop("hide");
		
		if (outEdges.length > 0)
		{
			pulseAnim.gotoAndPlay("show");
			transactionTextField.text = String(a_transaction);
			transactionTextField.textColor = 0x990000;
			
			for (var i=0; i<outEdges.length; i++)
			{
				outEdges[i].onPulse(a_transaction);
				_resetCounter++;
			}
		}
		else
		{
			reset();
			pulseAnim.gotoAndPlay("pulse");
		}
	}
	
	public function invalidatePulse(): Void
	{
		invalidateAnim.gotoAndPlay("show");
	}
	
	public function redrawEdges(): Void
	{
		for (var i=0; i<inEdges.length; i++)
			inEdges[i].redraw();
		
		for (var i=0; i<outEdges.length; i++)
			outEdges[i].redraw();
	}
	
	public function destroy(): Void
	{
		for (var i=0; i<inEdges.length; i++)
			inEdges[i].destroy();
		
		for (var i=0; i<outEdges.length; i++)
			outEdges[i].destroy();
			
		this.removeMovieClip();
	}
}
	