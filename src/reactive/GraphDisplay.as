﻿import reactive.NodeGroup;
import reactive.Node;
import reactive.Edge;

class reactive.GraphDisplay extends MovieClip
{
	public var edgePlane: MovieClip;
	public var nodePlane: MovieClip;
	public var threadPlane: MovieClip;
	public var dragArea: MovieClip;
	
	private var _nodeMap: Object;
	private var _edges: Object;
	
	private var _groups: Array;
	private var _isolatedGroup: NodeGroup;
	
	private var _threadMarkers: Array;
	
	public var debugModeEnabled: Boolean = false;
	
	
	public function GraphDisplay()
	{
		_nodeMap = {};
		
		_groups = [];
		_edges = [];
		
		_threadMarkers = [];
		
		_isolatedGroup = new NodeGroup(this, -1);
		_isolatedGroup.x = width/2;
		_isolatedGroup.y = height;
		_isolatedGroup.hAlign = "center";
		_isolatedGroup.vAlign = "bottom";
		_isolatedGroup.spacing = 10;
		
		dragArea.onPress = function()
		{
			_parent.startDrag();
		};
		
		dragArea.onRelease = function()
		{
			_parent.stopDrag();
			
			var p = {x:0, y:0};
			_parent.globalToLocal(p);
			
			this._x = p.x;
			this._y = p.y;
		};
		
		Mouse.addListener(this);
		
		setEdgeStyle(0);
	}
	
	public function onMouseWheel(delta): Void
	{
		nodePlane._xscale += delta * 2;
		nodePlane._yscale += delta * 2;
		
		edgePlane._xscale += delta * 2;
		edgePlane._yscale += delta * 2;
		
		threadPlane._xscale += delta * 2;
		threadPlane._yscale += delta * 2;
	}
	
	public function startTransaction(a_id: Number): Void
	{
	}
	
	public function endTransaction(a_id: Number): Void
	{
	}
	
	public function setEdgeStyle(a_style: Number): Void
	{
		edgePlane.edgeStyle = a_style;
	}
	
	public function showThreads(a_transaction: Number): Void
	{
		clearThreads();
		
		for (var s in _nodeMap)
		{
			var node = _nodeMap[s];
			
			var threadIdx = node.threadMap[a_transaction];
			if (threadIdx != null)
			{
				var depth = threadPlane.getNextHighestDepth();
				var t = threadPlane.attachMovie("ThreadMarker", "threadMarker" + depth, depth, {_x: node._x, _y: node._y});
				t.gotoAndStop(threadIdx + 1);
				
				_threadMarkers.push(t);
			}
			

		}
	}
	
	public function clearThreads(): Void
	{
		for (var i=0; i<_threadMarkers.length; i++)
		{
			_threadMarkers[i].removeMovieClip();
		}
	}
	
	public function clear(): Void
	{
		_groups.splice(0);
		
		for (var s in _nodeMap)
		{
			destroyNode(s);
		}
	}
	
	public function getNode(a_id: Number): Node
	{
		return _nodeMap[a_id];
	}
	
	public function createNode(a_nodeType: String, a_nodeId: Number, a_typeTag: String): Void
	{
		var node = nodePlane.attachMovie(a_nodeType, "Node" + a_nodeId, nodePlane.getNextHighestDepth(), {_x: 0, _y: 0, _width: 50, _height: 50, typeTag: a_typeTag});
		_nodeMap[a_nodeId] = node;
		node.id = a_nodeId;
		
		// Input/constant node?
		if (a_typeTag == "I" || a_typeTag == "C" || a_typeTag == "<<")
		{
			var group = getGroup(0);
			group.addNode(node);
			group.update();
		}
		else
		{
			_isolatedGroup.addNode(node);
			_isolatedGroup.update();
		}
		
		if (debugModeEnabled)
		{
			node.onRollOver = function()
			{
				var t = _parent.createTextField("infoText", _parent.getNextHighestDepth(), 0, 0, 500, 100);
	
				t.border = true;
				t.background = true;
				t.text = this.id;
				t.autoSize = true;
				
				var fmt:TextFormat = new TextFormat();
				fmt.size = 18;
				t.setTextFormat(fmt);
				
				t._x = this._x - t._width/2;
				t._y = this._y - 55;
			};
			
			node.onRollOut = function()
			{
				_parent.infoText.removeTextField();
			};
		}
	}
	
	public function destroyNode(a_nodeId: Number): Void
	{
		var node = _nodeMap[a_nodeId];
		node.destroy();
		
		_nodeMap[a_nodeId] = null;
	}
	
	public function attachNode(a_nodeId: Number, a_parentId: Number): Void
	{
		var node: Node = _nodeMap[a_nodeId];
		var parent: Node = _nodeMap[a_parentId];
		
		var edge = new Edge(parent, node, edgePlane);
		_edges.push(edge);
		
		parent.outEdges.push(edge);
		node.inEdges.push(edge);
		
		var curIndex = node.group != null ? node.group.index : -1;
		var newIndex = parent.group.index + 1;
		
		if (newIndex > curIndex)
		{
			var group = getGroup(newIndex);
			group.addNode(node);
			group.update();
			
			reorderChildren(node);
		}
	}
	
	private function reorderChildren(a_node: Node): Void
	{
		var parentIndex = a_node.group != null ? a_node.group.index : -1;
		
		for (var i=0; i<a_node.outEdges.length; i++)
		{
			var child: Node = a_node.outEdges[i].to;
			var childIndex = child.group != null ? child.group.index : -1;
			
			if (childIndex <= parentIndex)
			{
				var group = getGroup(parentIndex + 1);
				group.addNode(child);
				group.update();
				
				reorderChildren(child);

			}
		}
	}
	
	public function detachNode(a_nodeId: Number, a_parentId: Number): Void
	{
		var node: Node = _nodeMap[a_nodeId];
		var parent: Node = _nodeMap[a_parentId];
		
		for (var i=0; i<_edges.length; i++)
		{
			var edge = _edges[i];
			
			if (edge.from != parent || edge.to != node)
				continue;
				
			var idx: Number;
			
			idx = parent.outEdges.indexOf(edge);
			if (idx != undefined)
				parent.outEdges.splice(idx,1);
					
			idx = node.inEdges.indexOf(edge);
			if (idx != undefined)
				node.inEdges.splice(idx,1);

			_edges.splice(i,1);
			edge.destroy();
			
			break;
		}
	}
	
	private function getGroup(a_index): NodeGroup
	{
		if (_groups[a_index] == null)
		{
			var newGroup = new NodeGroup(this, a_index);
			newGroup.x = width / 2;
			newGroup.y = 100 * a_index;
			newGroup.hAlign = "center";
			newGroup.vAlign = "top";
			newGroup.spacing = 50;
			_groups[a_index] = newGroup;
		}
		
		return _groups[a_index];
	}
	

	
	public function get width(): Number
	{
		return nodePlane._width;
	}
	
	public function get height(): Number
	{
		return nodePlane._height;
	}
}