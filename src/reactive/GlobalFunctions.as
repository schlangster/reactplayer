﻿import skyui.defines.Input;

class reactive.GlobalFunctions
{
  /* PUBLIC FUNCTIONS */
	
	public static function extract(a_str: String, a_startChar: String, a_endChar: String): String
	{
		return a_str.slice(a_str.indexOf(a_startChar) + 1,a_str.lastIndexOf(a_endChar));
	}

	// Remove comments and leading/trailing white space
	public static function clean(a_str: String): String
	{
		if (a_str.indexOf(";") > 0)
			a_str = a_str.slice(0,a_str.indexOf(";"));

		var i = 0;
		while (a_str.charAt(i) == " " || a_str.charAt(i) == "\t")
			i++;

		var j = a_str.length - 1;
		while (a_str.charAt(j) == " " || a_str.charAt(j) == "\t")
			j--;

		return a_str.slice(i,j + 1);
	}

	public static function unescape(a_str: String): String 
	{
		a_str = a_str.split("\\n").join("\n");
		a_str = a_str.split("\\t").join("\t");
		return a_str;
	}

	private static var _arrayExtended = false;

	public static function addArrayFunctions(): Void
	{
		if (_arrayExtended)
			return;
			
		_arrayExtended = true;
		
		Array.prototype.indexOf = function (a_element): Number
		{
			for (var i=0; i<this.length; i++)
				if (this[i] == a_element)
					return i;
					
			return undefined;
		};
		
		Array.prototype.equals = function (a: Array): Boolean 
		{
			if (a == undefined)
				return false;
			
	    	if (this.length != a.length)
	        	return false;
			
	    	for (var i = 0; i < a.length; i++)
	        	if (a[i] !== this[i])
					return false;
					
	    	return true;
    	};
		
		Array.prototype.contains = function (a_element): Boolean 
		{
			for (var i=0; i<this.length; i++)
				if (this[i] == a_element)
					return true;
					
	    	return false;
    	};

    	_global.ASSetPropFlags(Array.prototype, ["indexOf", "equals", "contains"], 0x01, 0x00);
	}
}