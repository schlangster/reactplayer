﻿import reactive.LogParser;
import reactive.GraphDisplay;
import reactive.ControlShell;
import reactive.EventRecord;

import mx.utils.Delegate;

import flash.net.FileReference;

class reactive.ReactPlayer
{
	private var _curEvent: Number = 0;
	private var _curInterval: Number = 0;
	
	private var _events: Array;
	
	private var _intervalId: Number;
	
	private var _eventHandlers: Object;
	
	public var graphDisplay: GraphDisplay;
	
	private var _threadList: Array;
	private var _transactionList: Array;
	
	public var shell: ControlShell;
	
	public var activeLogName: String = "";
	public var loadingLogName: String = "";
	
	private var _stepMode: Boolean = false;
	
	public var timeField: TextField;
	public var turnField: TextField;
	
	private var _profiles: Object;
	
	private var _currentProfile: Object;
  
	public function ReactPlayer()
	{
		_threadList = [];
		_transactionList = [];
		_events = [];
		
		shell.player = this;
		
		_eventHandlers =
		{
			NodeCreate:			Delegate.create(this, processNodeCreate),
			NodeDestroy:		Delegate.create(this, processNodeDestroy),
			NodeAttach:			Delegate.create(this, processNodeAttach),
			NodeDetach:			Delegate.create(this, processNodeDetach),
			NodeEvaluateBegin:	Delegate.create(this, processNodeEvaluateBegin),
			NodeEvaluateEnd:	Delegate.create(this, processNodeEvaluateEnd),
			InputNodeAdmission:	Delegate.create(this, processInputNodeAdmission),
			NodeIdlePulse:		Delegate.create(this, processNodeIdlePulse),
			NodePulse:			Delegate.create(this, processNodePulse),
			NodeInvalidate:		Delegate.create(this, processNodeInvalidate),
			TransactionBegin:	Delegate.create(this, processTransactionBegin),
			TransactionEnd:		Delegate.create(this, processTransactionEnd),
			UserBreakpoint:		Delegate.create(this, processUserBreakpoint)
		};
		
		createProfiles();
	}
	
	// cmd interface
	
	public function cmdLoad(a_name: String): Void
	{
		loadingLogName = a_name + ".txt";
		LogParser.loadFile(loadingLogName, this, "onEventsParsed");
	}
	
	public function cmdReload(): Void
	{
		loadingLogName = activeLogName;
		LogParser.loadFile(loadingLogName, this, "onEventsParsed");
	}
	
	public function cmdPlay(a_profileName: String): Void
	{
		_stepMode = false;
		_currentProfile = _profiles[a_profileName];
		if (!_currentProfile)
			_currentProfile = _profiles["normal"];
		shell.printInfo("playing");
		clearInterval(_intervalId);
		_intervalId = setInterval(this, "processNextEvent", 0);
	}
	
	public function cmdPause(): Void
	{
		_stepMode = false;
		shell.printInfo("paused");
		clearInterval(_intervalId);
	}
	
	public function cmdStep(): Void
	{
		_stepMode = true;
		clearInterval(_intervalId);
		processNextEvent();
	}
	
	public function cmdDebugMode(a_enabled: Boolean): Void
	{
		shell.printInfo("debug mode: " + a_enabled);
		graphDisplay.debugModeEnabled = a_enabled;
	}
	
	public function cmdEdgeStyle(a_style: Number): Void
	{
		shell.printInfo("edge style: " + a_style);
		graphDisplay.setEdgeStyle(a_style);
	}
	
	public function cmdShowThreads(a_transaction: Number): Void
	{
		graphDisplay.showThreads(a_transaction);
	}
	
	public function cmdClearThreads(): Void
	{
		graphDisplay.clearThreads();
	}
	
	// ~cmd interface
	
	private function onEventsParsed(a_events: Array)
	{
		if (!a_events || a_events.length == 0)
		{
			shell.printError("failed to load '" + loadingLogName + "'");
			return;
		}
		
		activeLogName = loadingLogName;

		shell.printInfo("loaded '" + loadingLogName + "'");
		
		// Cleanup
		reset();
		
		_events = a_events;
	}
	
	private function reset()
	{
		clearInterval(_intervalId);
		_threadList.splice(0);
		_transactionList.splice(0);
		_events.splice(0);
		_curEvent = 0;
		_stepMode = false;

		graphDisplay.clear();

		timeField.text = "";
		turnField.text = "";
	}
	
	private function processNextEvent(): Void
	{
		clearInterval(_intervalId);
		
		if (_curEvent < _events.length)
		{
			var e = _events[_curEvent];
			trace(e);
			timeField.text = e.time + " time";
			_eventHandlers[e.name](e);
			
			_curEvent++;
			
			_curInterval = _currentProfile.intervals[e.name];
			if (_curInterval == -1)
				_stepMode = true;
			
			if (!_stepMode)
				_intervalId = setInterval(this, "processNextEvent", _curInterval);
			else
				shell.printInfo("step: " + e);
		}
	}
	
	private function processNodeCreate(a_event: EventRecord)
	{
		graphDisplay.createNode("Node", a_event.data.Node, getNodeTypeTag(a_event.data.Type));
	}
	
	private function processNodeDestroy(a_event: EventRecord)
	{
		graphDisplay.destroyNode(a_event.data.Node);
	}
	
	private function processNodeAttach(a_event: EventRecord)
	{
		graphDisplay.attachNode(a_event.data.Node, a_event.data.Parent);
	}
	
	private function processNodeDetach(a_event: EventRecord)
	{
		graphDisplay.detachNode(a_event.data.Node, a_event.data.Parent);
	}
	
	private function processInputNodeAdmission(a_event: EventRecord)
	{
		var node = graphDisplay.getNode(a_event.data.Node);
		if (node != null)
		{
			turnField.text = getTransactionNum(a_event.data.Transaction) + " turn";
			node.setAdmission(a_event.data.Transaction);
		}
	}
	
	private function processNodeIdlePulse(a_event: EventRecord)
	{
		var node = graphDisplay.getNode(a_event.data.Node);
		if (node != null)
		{
			node.idlePulse(a_event.data.Transaction);
		}
	}
	
	private function processNodePulse(a_event: EventRecord)
	{
		var node = graphDisplay.getNode(a_event.data.Node);
		if (node != null)
		{
			node.pulse(a_event.data.Transaction);
		}
	}
	
	private function processNodeInvalidate(a_event: EventRecord)
	{
		graphDisplay.detachNode(a_event.data.Node, a_event.data.OldParent);
		graphDisplay.attachNode(a_event.data.Node, a_event.data.NewParent);
		
		var node = graphDisplay.getNode(a_event.data.Node);
		if (node != null)
		{
			node.invalidatePulse();
		}
	}
	
	private function processNodeEvaluateBegin(a_event: EventRecord)
	{
		var node = graphDisplay.getNode(a_event.data.Node);
		if (node != null)
		{
			node.startProcessing(a_event.data.Transaction, getThreadNum(a_event.data.Thread));
			
//			graphDisplay.markTransaction(a_event.data.Transaction, a_event.data.Node);
		}
	}
	
	private function processNodeEvaluateEnd(a_event: EventRecord)
	{
		var node = graphDisplay.getNode(a_event.data.Node);
		if (node != null)
		{
			node.stopProcessing(1);
		}
	}
	
	private function processTransactionBegin(a_event: EventRecord)
	{
		var node = graphDisplay.startTransaction();
	}
	
	private function processTransactionEnd(a_event: EventRecord)
	{
	}
	
	private function processUserBreakpoint(a_event: EventRecord)
	{
	}
	
	private function getNodeTypeTag(a_type: String): String
	{
		switch (a_type)
		{
			// Input nodes
			case "VarNode":
				return "I";
			case "ValNode":
				return "C";				
			case "EventSourceNode":
				return "<<";
				
			// Output nodes
			case "SignalObserverNode":
			case "EventObserverNode":
				return "O";
			
			// Signals
			case "FunctionNode":
				return "F";
			case "FlattenNode":
				return "FL";
				
			// Events
			case "EventFilterNode":
				return "==";
			case "EventTransformNode":
				return "->";
				
			// Conversions
			case "FoldNode":
				return "FO";
			case "IterateNode":
				return "IT";
			case "PulseNode":
				return "PU";
			case "MonitorNode":
				return "MO";




			default:
				return "?";
		}
	}
	
	private function getThreadNum(a_threadId: Number): Number
	{
		var result = _threadList.indexOf(a_threadId);
		if (_threadList.indexOf(a_threadId) == undefined)
		{
			_threadList.push(a_threadId);
			result = _threadList.length-1;
		}
		return result;
	}
	
	private function getTransactionNum(a_transactionId: Number): Number
	{
		var result = _transactionList.indexOf(a_transactionId);
		if (_transactionList.indexOf(a_transactionId) == undefined)
		{
			_transactionList.push(a_transactionId);
			result = _transactionList.length-1;
		}
		return result;
	}
	
	private function createProfiles(): Void
	{
		_profiles =
		{
			normal:
			{
				intervals:
				{
					NodeCreate:			2,
					NodeDestroy:		2,
					NodeAttach:			2,
					NodeDetach:			2,
					NodeEvaluateBegin:	1000,
					NodeEvaluateEnd:	100,
					InputNodeAdmission:	100,
					NodeIdlePulse:		500,
					NodePulse:			500,
					NodeInvalidate:		500,
					TransactionBegin:	2,
					TransactionEnd:		1000,
					UserBreakpoint:		-1
				}
			},
			verbose:
			{
				intervals:
				{
					NodeCreate:			500,
					NodeDestroy:		500,
					NodeAttach:			500,
					NodeDetach:			500,
					NodeEvaluateBegin:	2000,
					NodeEvaluateEnd:	1000,
					InputNodeAdmission:	1000,
					NodeIdlePulse:		1000,
					NodePulse:			1000,
					NodeInvalidate:		1000,
					TransactionBegin:	500,
					TransactionEnd:		-1,
					UserBreakpoint:		-1
				}
			},
			fast:
			{
				intervals:
				{
					NodeCreate:			2,
					NodeDestroy:		2,
					NodeAttach:			2,
					NodeDetach:			2,
					NodeEvaluateBegin:	500,
					NodeEvaluateEnd:	100,
					InputNodeAdmission:	100,
					NodeIdlePulse:		250,
					NodePulse:			250,
					NodeInvalidate:		250,
					TransactionBegin:	2,
					TransactionEnd:		100,
					UserBreakpoint:		-1
				}
			},
			faster:
			{
				intervals:
				{
					NodeCreate:			2,
					NodeDestroy:		2,
					NodeAttach:			2,
					NodeDetach:			2,
					NodeEvaluateBegin:	200,
					NodeEvaluateEnd:	2,
					InputNodeAdmission:	100,
					NodeIdlePulse:		100,
					NodePulse:			100,
					NodeInvalidate:		100,
					TransactionBegin:	2,
					TransactionEnd:		100,
					UserBreakpoint:		-1
				}
			},
			forward:
			{
				intervals:
				{
					NodeCreate:			2,
					NodeDestroy:		2,
					NodeAttach:			2,
					NodeDetach:			2,
					NodeEvaluateBegin:	2,
					NodeEvaluateEnd:	2,
					InputNodeAdmission:	2,
					NodeIdlePulse:		2,
					NodePulse:			2,
					NodeInvalidate:		2,
					TransactionBegin:	2,
					TransactionEnd:		-1,
					UserBreakpoint:		-1
				}
			}
		};
	}
}