﻿import reactive.GlobalFunctions;
import reactive.EventRecord;

class reactive.LogParser
{
  /* PRIVATE VARIABLES */
  
	private static var _callbackObj: Object = null;
	private static var _callbackName: String = "";
  
	
  /* PUBLIC FUNCTIONS */
  
	public static function loadFile(a_file: String, a_callbackObj: Object, a_callbackName: String): Void
	{
		GlobalFunctions.addArrayFunctions();
		
		_callbackObj = a_callbackObj;
		_callbackName = a_callbackName;
		
		var lv = new LoadVars();
		lv.onData = parseData;
		lv.load(a_file);
	}

  /* PRIVATE FUNCTIONS */

	private static function parseData(a_data:Array): Void
	{
		var lines = a_data.split("\r\n");
		if (lines.length == 1)
			lines = a_data.split("\n");

		var e = null;
		var events = [];

		for (var i = 0; i < lines.length; i++)
		{
			var line = lines[i];
			
			if (lines[i].length < 1)
				continue;
			
			// Comment
			if (lines[i].charAt(0) == ";")
				continue;
			
			// Event
			if (lines[i].charAt(0) != ">")
			{
				
				// Previous event is done
				if (e)
				{
					events.push(e);
					e = null;
				}

				var eventName = GlobalFunctions.clean(lines[i].slice(0, lines[i].indexOf(":")));
				var time = Number(GlobalFunctions.clean(lines[i].slice(lines[i].indexOf(":") + 1)));

				e = new EventRecord(eventName, time);

			// Data
			}
			else
			{
				var key = GlobalFunctions.clean(lines[i].slice(1, lines[i].indexOf("=")));
				var val = parseValueString(GlobalFunctions.clean(lines[i].slice(lines[i].indexOf("=") + 1)));
				e.data[key] = val;
			}
		}
		
		// Don't forget last event
		if (e)
		{
			events.push(e);
			e = null;
		}
		
		_callbackObj[_callbackName](events);
	}
	
	private static function parseValueString(a_str: String): Object
	{
		if (a_str == undefined)
			return undefined;
			
		var t = undefined;

		// Number?
		if (!isNaN(a_str))
		{
			return Number(a_str);
			
		// Bool true?
		}
		else if (a_str.toLowerCase() == "true")
		{
			return true;
			
		// Bool false?
		}
		else if (a_str.toLowerCase() == "false")
		{
			return false;

		// Undefined?
		}
		else if (a_str.toLowerCase() == "undefined")
		{
			return undefined;
		}
		
		// Default String
		return a_str;
	}
}