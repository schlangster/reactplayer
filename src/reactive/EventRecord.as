﻿class reactive.EventRecord
{
	public var name: String;
	public var time: Number;

	public var data: Object;
	
	public function EventRecord(a_name: String, a_time: Number)
	{
		name = a_name;
		time = a_time;
		
		data = {};
	}
	
	public function toString(): String
	{
		var s = name;
		for (var k in data)
			s += " [" + k + " = " + data[k] + "]";
			
		return s;
	}
}