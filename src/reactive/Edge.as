﻿import reactive.Node;

class reactive.Edge
{
	public static var COLOR_DEFAULT		= 0x000000;
	public static var COLOR_IDLEPULSED	= 0xEBEBEB;
	public static var COLOR_PULSED		= 0x990000;
	
	public var from: Node;
	public var to: Node;
	public var line: MovieClip;

	private var _state: Number = 0;
	
	public function Edge(a_from: Node, a_to: Node, a_canvas: MovieClip)
	{
		from = a_from;
		to = a_to;
		
		drawLine(a_canvas);
	}
	
	public function redraw(): Void
	{
		if (line)
		{
			var canvas = line._parent;
			line.removeMovieClip();
			
			drawLine(canvas);
		}
	}
	
	public function onReset(): Void
	{
		_state = 0;
		redraw();
		
		from.onEdgeReset();
	}
	
	public function onIdlePulse(): Void
	{
		if (_state < 1)
		{
			_state = 1;
			redraw();
		}
	}
	
	public function onPulse(a_transaction: Number): Void
	{
		if (_state < 2)
		{
			_state = 2;
			redraw();
		}
	}
	
	private function drawLine(a_canvas: MovieClip): Void
	{
		var lineWidth: Number;
		var color: Number;
		
		if (_state == 0)
		{
			lineWidth = 2;
			color = COLOR_DEFAULT;
		}
		else if (_state == 1)
		{
			lineWidth = 4;
			color = COLOR_IDLEPULSED;
		}
		else
		{
			lineWidth = 4;
			color = COLOR_PULSED;
		}
		
		var d = to.group.index - from.group.index
		
		var depth = a_canvas.getNextHighestDepth();
		line = a_canvas.createEmptyMovieClip("edge" + depth, depth);
		line.lineStyle(lineWidth, color, 100, true, "normal", "square", "miter");
			line.moveTo(from._x, from._y);
		
		if (d == 1 || a_canvas.edgeStyle == 0)
		{
			line.lineTo(to._x, to._y);
		}
		else
		{
			// This has to do for now :)
			var j = a_canvas.edgeStyle == 1 ? 1 : -1;
			var k = to._x > from._x ? -50 : 50;
			var px = from._x + ((to._x - from._x) / 2) - d * k * j;
			var py = from._y + ((to._y - from._y) / 2);
			line.curveTo(px, py ,to._x, to._y);
		}
	}
	
	public function destroy(): Void
	{
		if (line)
			line.removeMovieClip();
		from = null;
		to = null;
	}
}