﻿import reactive.Node;
import reactive.GraphDisplay;

class reactive.NodeGroup
{
	public var background: MovieClip;
	
	private var _nodes: Array;

	public var 	index: Number;
	
	public var x: Number = 0;
	public var y: Number = 0;
	
	public var spacing: Number = 10;
	
	public var vAlign: String = "top";
	public var hAlign: String = "right";
	
	public var nodeSize: Number = 50;
	
	private var _display: GraphDisplay;
	
	public function NodeGroup(a_display: GraphDisplay, a_index: Number)
	{
		_nodes = [];
		index = a_index;
		
		_display = a_display;
	}
	
	public function addNode(a_node: Node): Void
	{
		var oldGroup = a_node.group;
		
		if (oldGroup == this)
			return;
			
		if (oldGroup != null)
			oldGroup.removeNode(a_node);			
		
		a_node.group = this;
		_nodes.push(a_node);
	}
	
	public function removeNode(a_node: Node): Void
	{
		var i = _nodes.indexOf(a_node);
		if (i > -1)
		{
			a_node.group = null;
			_nodes.splice(i);
		}
	}
	
	public function update(): Void
	{
		var curX = x;
		var curY = y;
		
		if (hAlign == "right")
			curX -= width + nodeSize;
		else if (hAlign == "center")
			curX -= width/2 + nodeSize/2;
		else
			curX += nodeSize/2;
			
		if (vAlign == "bottom")
			curY -= height - nodeSize/2;
		else if (vAlign == "center")
			curY -= height/2 + nodeSize/2;
		else
			curY += nodeSize/2
		
		for (var i=0; i<_nodes.length; i++)
		{
			var node = _nodes[i];
		
			
			node._x = curX;
			node._y = curY;
			
			node.redrawEdges();
			
			curX += nodeSize + spacing;
		}
	}
	
	public function get width(): Number
	{
		return _nodes.length * nodeSize + (_nodes.length - 1) * spacing;
	}
	
	public function get height(): Number
	{
		return nodeSize;
	}
}